package main

import (
	_ "godocker/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

