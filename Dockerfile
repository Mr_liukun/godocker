FROM golang:1.10.7

#创建工作目录
RUN mkdir -p /go/src/godocker

#进入工作目录
WORKDIR /go/src/godocker

#将当前目录下的所有文件复制到指定位置
COPY . /go/src/godocker

#下载beego和bee
RUN go get github.com/astaxie/beego && go get github.com/beego/bee

#暴露的端口
EXPOSE 8080

#运行
CMD ["bee", "run"]


